﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterSetting.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using ElzabException;

    /// <summary>
    /// The Part of the class describes settings.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        /// <summary>
        /// Gets the unique number fiscal printer (read-only)
        /// </summary>
        public override void GetUniqueNumberDevice()
        {
            StringBuilder uniqueNumber = new StringBuilder();
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(pReadUniqueNumber(uniqueNumber));
            CommunicationEnd();

            UniqueNumber = uniqueNumber.ToString();
        }

        /// <summary>
        /// Gets the name of the device.
        /// </summary>
        public void GetDeviceName()
        {
            StringBuilder deviceName = new StringBuilder();

            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(pDeviceName(deviceName));
            CommunicationEnd();

            DeviceName = deviceName.ToString();
        }

        /// <summary>
        /// Prints the status device.
        /// </summary>
        public void PrintStatusDevice()
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(PrinterStatusReport());
            CommunicationEnd();
        }

        /// <summary>
        /// Gets the fiscal printer time.
        /// </summary>
        /// <returns>
        /// The fiscal printer time
        /// </returns>
        public override DateTime GetDeviceTime()
        {
            StringBuilder time = new StringBuilder();

            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(pReadClock(time));
            CommunicationEnd();

            return DateTime.ParseExact("17.03.28;19:55", "yy.MM.dd;HH:mm", CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Changes the time on fiscal printer.
        /// </summary>
        /// <param name="clock">The current time.</param>
        public override void ChangeTime(DateTime clock)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ChangeTime(clock.Hour, clock.Minute));
            CommunicationEnd();
        }

        /// <summary>
        /// Gets the device name and version.
        /// </summary>
        public void GetDeviceNameVersion()
        {
            CommunicationInit(Port, Speed, Timeout);

            unsafe
            {
                int appVersion;
                Device deviceType;

                VerifyImportedFunction.CheckForExceptions(DeviceType(&deviceType, &appVersion));

                DeviceName = deviceType.ToString();
                DeviceAppVersion = appVersion.ToString();
            }

            CommunicationEnd();
        }

        /// <summary>
        /// Gets the Elzab library version.
        /// </summary>
        /// <returns>The library version.</returns>
        public string GetElzabLibraryVersion()
        {
            StringBuilder versionDll = new StringBuilder();
            VerifyImportedFunction.CheckForExceptions(pDllVersion(versionDll));

            return versionDll.ToString();
        }

        /// <summary>
        /// Gets the Elzab library author.
        /// </summary>
        /// <returns>The author library.</returns>
        public string GetElzabLibraryAuthor()
        {
            StringBuilder authorDll = new StringBuilder();
            VerifyImportedFunction.CheckForExceptions(pDllAuthor(authorDll));

            return authorDll.ToString();
        }

        /// <summary>
        /// Reads the vat rate from device.
        /// </summary>
        /// <returns>The list of the vat rate.</returns>
        public Dictionary<string, int> ReadVatFromDevice()
        {
            Dictionary<string, int> listVat = new Dictionary<string, int>();

            CommunicationInit(Port, Speed, Timeout);
            unsafe
            {
                int ile = 7;
                int a, b, c, d, e, f, g;

                VerifyImportedFunction.CheckForExceptions(ReadVAT(&ile, &a, &b, &c, &d, &e, &f, &g));

                listVat.Add("A", a);
                listVat.Add("B", b);
                listVat.Add("C", c);
                listVat.Add("D", d);
                listVat.Add("E", e);
                listVat.Add("F", f);
                listVat.Add("G", g);
            }

            CommunicationEnd();

            return listVat;
        }

        /// <summary>
        /// Writes the vat rate to device.
        /// </summary>
        /// <param name="listVat">The list of the vat rate.</param>
        public void SetVatToDevice(Dictionary<string, int> listVat)
        {
            CommunicationInit(Port, Speed, Timeout);
            unsafe
            {
                int* countVAT;
                int listCount = listVat.Count;
                countVAT = &listCount;
                VerifyImportedFunction.CheckForExceptions(SetVAT(countVAT, listVat["A"], listVat["B"], listVat["C"], listVat["D"], listVat["E"], listVat["F"], listVat["G"]));
            }

            CommunicationEnd();
        }

        /// <summary>
        /// Turns off the fiscal printer.
        /// </summary>
        public void TurnOffFiscalPrinter()
        {
            byte[] inputBuffer = new byte[0];
            byte[] outputBuffer = { 0x30, 0x30, 0x30, 0x30, 0x30 };

            OwnRSSequences(0xFD, 0, 5, inputBuffer, outputBuffer);
        }

        /// <summary>
        /// Create own the RSS sequence which it sends to fiscal printer.
        /// </summary>
        /// <param name="sequence">The number sequence.</param>
        /// <param name="quantityOfBytesToReceive">The quantity of bytes to receive.</param>
        /// <param name="quantityOfBytesToSend">The quantity of bytes to send.</param>
        /// <param name="inputBuffer">The input buffer array.</param>
        /// <param name="outputBuffer">The output buffer array.</param>
        public void OwnRSSequences(int sequence, int quantityOfBytesToReceive, int quantityOfBytesToSend, byte[] inputBuffer, byte[] outputBuffer)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(RSSequence(sequence, quantityOfBytesToReceive, quantityOfBytesToSend, inputBuffer, outputBuffer));
            CommunicationEnd();
        }
    }
}
