﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterMain.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    /// <summary>
    /// The Part of the class describes the initialize instance.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter : FiscalPrinter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinter"/> class.
        /// </summary>
        public ElzabFiscalPrinter()
        {  
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinter"/> class.
        /// </summary>
        /// <param name="port">The COM port.</param>
        public ElzabFiscalPrinter(int port)
        {
            Port = port;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinter"/> class.
        /// </summary>
        /// <param name="ip">The IP address.</param>
        public ElzabFiscalPrinter(string ip)
        {
            IP = ip;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinter"/> class.
        /// </summary>
        /// <param name="port">The COM port.</param>
        /// <param name="speed">The speed.</param>
        /// <param name="timeout">The timeout.</param>
        public ElzabFiscalPrinter(int port, int speed, int timeout)
        {
            Port = port;
            Speed = speed;
            Timeout = timeout;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinter"/> class.
        /// </summary>
        /// <param name="ip">The IP address.</param>
        /// <param name="tcpPort">The TCP port.</param>
        /// <param name="timeout">The timeout.</param>
        public ElzabFiscalPrinter(string ip, int tcpPort, int timeout)
        {
            IP = ip;
            TcpPort = tcpPort;
            Timeout = timeout;
        }

        /// \example Examples\PrintReceipt.cs
        /// \example Examples\PrintReceiptWithDiscount.cs
        /// \example Examples\PrintInvoice.cs
        /// \example Examples\DayReport.cs
        /// \example Examples\SetVatToDevice.cs
        /// \example Examples\OwnRSSequences.cs
    }
}
