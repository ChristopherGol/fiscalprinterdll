﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterReport.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System;
    using ElzabException;

    /// <summary>
    /// The Part of the class describes report.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        /// <summary>
        /// Print the day report.
        /// </summary>
        /// <param name="ifSale">Was there sales during the day.</param>
        public override void DayReport(object ifSale)
        {
            CommunicationInit(Port, Speed, Timeout);

            switch ((IfSale)ifSale)
            {
                case IfSale.No:
                    VerifyImportedFunction.CheckForExceptions(DailyReport(1));
                    break;
                case IfSale.Yes:
                    VerifyImportedFunction.CheckForExceptions(DailyReport(0));
                    break;
                default:
                    VerifyImportedFunction.CheckForExceptions(DailyReport(0));
                    break;
            }

            CommunicationEnd();
        }

        /// <summary>
        /// Prints the month report.
        /// </summary>
        /// <param name="type">The type report (detailed or shortened).</param>
        /// <param name="month">The month and the year report.</param>
        public override void MonthReport(object type, DateTime month)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(MonthlyReport((int)((ReportFiscalType)type), month.Year, month.Month));
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the report by numbers.
        /// </summary>
        /// <param name="type">The type report (detailed or shortened).</param>
        /// <param name="startNumber">The start number report.</param>
        /// <param name="endNumber">The end number report.</param>
        public override void NumberReport(object type, int startNumber, int endNumber)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(NumberReport((int)((ReportFiscalType)type), startNumber, endNumber));
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the report by periods.
        /// </summary>
        /// <param name="type">The type report (detailed or shortened).</param>
        /// <param name="time1">Since when report</param>
        /// <param name="time2">Until when report</param>
        public override void PeriodReport(object type, DateTime time1, DateTime time2)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(PeriodReport((int)((ReportFiscalType)type), time1.Year, time1.Month, time1.Day, time2.Year, time2.Month, time2.Day));
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the locked product on fiscal printer.
        /// </summary>
        public void PrintLockedProduct()
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(LockedArticlesReport());
            CommunicationEnd();
        }

        /// <summary>
        /// Gets the last printed receipt number.
        /// </summary>
        /// <returns>The last receipt number.</returns>
        public int GetLastReceiptNumber()
        {
            int lastReceiptNumber = 0;

            CommunicationInit(Port, Speed, Timeout);
            unsafe
            {
                VerifyImportedFunction.CheckForExceptions(ReceiptNumber(&lastReceiptNumber));
            }

            CommunicationEnd();

            return lastReceiptNumber;
        }

        /// <summary>
        /// Gets the last printed daily report number.
        /// </summary>
        /// <returns>The last daily report number.</returns>
        public int GetLastDayReportNumber()
        {
            CommunicationInit(Port, Speed, Timeout);

            int lastDayReportNumber = 0;

            unsafe
            {
                VerifyImportedFunction.CheckForExceptions(DailyReportNumber(&lastDayReportNumber));
            }

            CommunicationEnd();

            return lastDayReportNumber;
        } 
    }
}
