﻿//-----------------------------------------------------------------------
// <copyright file="Enums.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    /// <summary>
    /// The type of connection with fiscal printer
    /// </summary>
    public enum TypeConnection
    {
        /// <summary>
        /// The connection over COM port
        /// </summary>
        COM = 0,

        /// <summary>
        /// The connection over LAN network
        /// </summary>
        Network = 1
    }

    /// <summary>
    /// The Type of fiscal printer
    /// </summary>
    public enum Device
    {
        /// <summary>
        /// FP6A5ST model
        /// </summary>
        Fp6A5St = 1,

        /// <summary>
        /// FP6A7ST model
        /// </summary>
        Fp6A7St = 2,

        /// <summary>
        /// Omega1 model
        /// </summary>
        Omega1 = 3,

        /// <summary>
        /// Omega2 model
        /// </summary>
        Omega2 = 4,

        /// <summary>
        /// FP6B5ST model
        /// </summary>
        Fp6B5St = 5,

        /// <summary>
        /// FP6B7ST model
        /// </summary>
        Fp6B7St = 6,

        /// <summary>
        /// OmegaF model
        /// </summary>
        OmegaF = 7,

        /// <summary>
        /// Mera model
        /// </summary>
        Mera = 8,

        /// <summary>
        /// Mera F model
        /// </summary>
        MeraF = 9,

        /// <summary>
        /// Mera E model
        /// </summary>
        MeraE = 10,

        /// <summary>
        /// Mera EFV model
        /// </summary>
        MeraEfv = 11,

        /// <summary>
        /// Mera FE model
        /// </summary>
        MeraFe = 12,

        /// <summary>
        /// Mera TE model
        /// </summary>
        MeraTe = 13,

        /// <summary>
        /// Mera TEFV model
        /// </summary>
        MeraTefv = 14,

        /// <summary>
        /// Zeta model
        /// </summary>
        Zeta = 15,

        /// <summary>
        /// D10 model
        /// </summary>
        D10 = 16,

        /// <summary>
        /// The newest model
        /// </summary>
        Najnowsze = 29
    }

    /// <summary>
    /// Was there sales during the current day.
    /// </summary>
    public enum IfSale
    {
        /// <summary>
        /// It was sale during the day.
        /// </summary>
        Yes = 0,

        /// <summary>
        /// It was not sale during the day.
        /// </summary>
        No = 1,
    }

    /// <summary>
    /// The tax rate. It can change assigning rate to values
    /// </summary>
    public enum VatRate
    {
        /// <summary>
        /// 23% vat rate
        /// </summary>
        A = 1,

        /// <summary>
        /// 8% vat rate
        /// </summary>
        B = 2,

        /// <summary>
        /// 0% vat rate
        /// </summary>
        C = 3,

        /// <summary>
        /// 5% vat rate
        /// </summary>
        D = 4,

        /// <summary>
        /// E - Reserve
        /// </summary>
        E = 5,

        /// <summary>
        /// F - Reserve
        /// </summary>
        F = 6,

        /// <summary>
        /// G - Released from vat rate
        /// </summary>
        G = 7,
    }

    /// <summary>
    /// The message on the position receipt.
    /// </summary>
    public enum MessageReceipt
    {
        /// <summary>
        /// The lack of message
        /// </summary>
        BrakKomunikatu = 0,

        /// <summary>
        /// With discount
        /// </summary>
        ZRabatem = 1,

        /// <summary>
        /// The discount
        /// </summary>
        Przecena = 2,

        /// <summary>
        /// The discount
        /// </summary>
        Promocja = 3,

        /// <summary>
        /// With the bail
        /// </summary>
        ZKaucja = 4,

        /// <summary>
        /// Without the bail
        /// </summary>
        BezKaucji = 5,

        /// <summary>
        /// The first price
        /// </summary>
        Cena1 = 6,

        /// <summary>
        /// The second price
        /// </summary>
        Cena2 = 7,
    }

    /// <summary>
    /// The type of discount.
    /// </summary>
    public enum TypeDiscountSurcharge
    {
        /// <summary>
        /// The price increases
        /// </summary>
        Surcharge = 0,

        /// <summary>
        /// The price decreases
        /// </summary>
        Discount = 1,
    }

    /// <summary>
    /// The type of discount in view of calculation method discount
    /// </summary>
    public enum TypeDiscount
    {
        /// <summary>
        /// No discount
        /// </summary>
        None = -1,

        /// <summary>
        /// The percentage discount
        /// </summary>
        Percentage = 0,

        /// <summary>
        /// The cost discount
        /// </summary>
        Cost = 1,
    }

    /// <summary>
    /// The type of fiscal report.
    /// </summary>
    public enum ReportFiscalType
    {
        /// <summary>
        /// The detailed report
        /// </summary>
        Szczegolowy = 1,

        /// <summary>
        /// The shortened report
        /// </summary>
        Skrocony = 0,
    }

    /// <summary>
    /// The additional lines on the end receipt (after bill logo)
    /// </summary>
    public enum AdditionalLinesReceipt
    {
        /// <summary>
        /// The paid
        /// </summary>
        Zaplacono = 0,

        /// <summary>
        /// The rest
        /// </summary>
        Reszta = 1,

        /// <summary>
        /// The method of payment
        /// </summary>
        SposobZaplaty = 2,

        /// <summary>
        /// Welcome for example again
        /// </summary>
        Zapraszamy = 3,

        /// <summary>
        /// The opening hours
        /// </summary>
        Otwarte = 4,

        /// <summary>
        /// The attendant
        /// </summary>
        Obslugujacy = 5,

        /// <summary>
        /// The computer
        /// </summary>
        Komputerowy = 6,

        /// <summary>
        /// The voucher
        /// </summary>
        Voucher = 7,

        /// <summary>
        /// The bail
        /// </summary>
        Kaucja = 8,

        /// <summary>
        /// The return the bail
        /// </summary>
        ZwrotKaucji = 9,

        /// <summary>
        /// The cash
        /// </summary>
        Gotowka = 10,

        /// <summary>
        /// No overhead
        /// </summary>
        BezNarzutu = 11,

        /// <summary>
        /// The number
        /// </summary>
        Numer = 12,

        /// <summary>
        /// The counter state
        /// </summary>
        StanLicznika = 13,

        /// <summary>
        /// The customer
        /// </summary>
        Klient = 14,

        /// <summary>
        /// The credit card
        /// </summary>
        KartaKredytowa = 15,

        /// <summary>
        /// The number of items
        /// </summary>
        IloscPozycji = 16,

        /// <summary>
        /// The barcode
        /// </summary>
        DrukKoduKreskowego = 17,

        /// <summary>
        /// We do not accept returns
        /// </summary>
        ZwrotowNiePrzyjmujemy = 18,

        /// <summary>
        /// The scores
        /// </summary>
        Punkty = 19,

        /// <summary>
        /// The sum of points
        /// </summary>
        SumaPunktow = 20,

        /// <summary>
        /// The bonus
        /// </summary>
        Bonifikata = 21,

        /// <summary>
        /// After the bonus
        /// </summary>
        PoBonifikacie = 22
    }

    public enum NonFiscalDocument
    {
        /// <summary>
        /// The stan kasy
        /// </summary>
        Stan_kasy = 10,

        /// <summary>
        /// The pokwitowanie wplaty
        /// </summary>
        Pokwitowanie_wplaty = 20,

        /// <summary>
        /// The pokwitowanie wyplaty
        /// </summary>
        Pokwitowanie_wyplaty = 30,

        /// <summary>
        /// The biezacy raport kasowy
        /// </summary>
        Biezacy_raport_kasowy = 40,

        /// <summary>
        /// The raport kasowy zmianowy
        /// </summary>
        Raport_kasowy_zmianowy = 50,

        /// <summary>
        /// The zamowienie
        /// </summary>
        Zamowienie = 70,

        /// <summary>
        /// The potwierdzenie zaplaty karta platnicza
        /// </summary>
        Potwierdzenie_zaplaty_karta_platnicza = 80,

        /// <summary>
        /// The pokwitowanie anulowanie platnosci karta foliowa
        /// </summary>
        Pokwitowanie_anulowanie_platnosci_karta_foliowa = 90,

        /// <summary>
        /// The raport kasowy
        /// </summary>
        Raport_kasowy = 01,

        /// <summary>
        /// The transakcje odlozone
        /// </summary>
        Transakcje_odlozone = 11,

        /// <summary>
        /// The raport funkcji operatora
        /// </summary>
        Raport_funkcji_operatora = 21,

        /// <summary>
        /// The raport srodkow platnosci
        /// </summary>
        Raport_srodkow_platnosci = 31,

        /// <summary>
        /// The raport sprzedazy
        /// </summary>
        Raport_sprzedazy = 41,

        /// <summary>
        /// The pokwitowanie
        /// </summary>
        Pokwitowanie = 51,

        /// <summary>
        /// The przedplata zamowienie
        /// </summary>
        Przedplata_zamowienie = 61,

        /// <summary>
        /// The bon upominkowy
        /// </summary>
        Bon_upominkowy = 71,

        /// <summary>
        /// The kupon rabatowy
        /// </summary>
        Kupon_rabatowy = 71,

        /// <summary>
        /// The platnosc za pobraniem
        /// </summary>
        Platnosc_za_pobraniem = 81,

        /// <summary>
        /// The przelew bankowy
        /// </summary>
        Przelew_bankowy = 91,

        /// <summary>
        /// The rabat dla pracownika
        /// </summary>
        Rabat_dla_pracownika = 02,

        /// <summary>
        /// The raport towarow
        /// </summary>
        Raport_towarow = 12,

        /// <summary>
        /// The lista kasjerow
        /// </summary>
        Lista_kasjerow = 22,

        /// <summary>
        /// The raport godzinowy
        /// </summary>
        Raport_godzinowy = 32,

        /// <summary>
        /// The rozliczenie kasjera
        /// </summary>
        Rozliczenie_kasjera = 42,

        /// <summary>
        /// The stany licznikow
        /// </summary>
        Stany_licznikow = 52,

        /// <summary>
        /// The raport tankowania
        /// </summary>
        Raport_tankowania = 62,

        /// <summary>
        /// The waluta w sejfie
        /// </summary>
        Waluta_w_sejfie = 72,

        /// <summary>
        /// The raport alarmu paliwa
        /// </summary>
        Raport_alarmu_paliwa = 82,

        /// <summary>
        /// The bilet do myjni
        /// </summary>
        Bilet_do_myjni = 92,

        /// <summary>
        /// The raport stanu zbiornika
        /// </summary>
        Raport_stanu_zbiornika = 03,

        /// <summary>
        /// The raport dostawy paliw
        /// </summary>
        Raport_dostawy_paliw = 13,

        /// <summary>
        /// The potwierdzenie doladowania numeru GSM
        /// </summary>
        Potwierdzenie_doladowania_numeru_GSM = 23,

        /// <summary>
        /// The raport kasowy v2
        /// </summary>
        Raport_kasowy_v2 = 33,

        /// <summary>
        /// The pokwitowanie zwrotu
        /// </summary>
        Pokwitowanie_zwrotu = 43,

        /// <summary>
        /// The zamowienie mera
        /// </summary>
        Zamowienie_Mera = 53,

        /// <summary>
        /// The raport funkcji operatora mera
        /// </summary>
        Raport_funkcji_operatora_Mera = 63,

        /// <summary>
        /// The operacje kasowe
        /// </summary>
        Operacje_kasowe = 73,

        /// <summary>
        /// The raport zmianowy
        /// </summary>
        Raport_zmianowy = 83,

        /// <summary>
        /// The potwierdzenie
        /// </summary>
        Potwierdzenie = 93,

        /// <summary>
        /// The raport
        /// </summary>
        Raport = 93,

        /// <summary>
        /// The bon
        /// </summary>
        Bon = 93,

        /// <summary>
        /// The prepaid
        /// </summary>
        Prepaid = 04,

        /// <summary>
        /// The potwierdzenie platnosci za rachunek
        /// </summary>
        Potwierdzenie_platnosci_za_rachunek = 14,

        /// <summary>
        /// The anulowanie platnosci za rachunek
        /// </summary>
        Anulowanie_platnosci_za_rachunek = 14,

        /// <summary>
        /// The transakcja lojalnosciowa
        /// </summary>
        Transakcja_lojalnosciowa = 24,

        /// <summary>
        /// The potwierdzenie zaplaty karta
        /// </summary>
        Potwierdzenie_zaplaty_karta = 34,

        /// <summary>
        /// The raport kasjera
        /// </summary>
        Raport_kasjera = 44,

        /// <summary>
        /// The potwierdzenie zaplaty
        /// </summary>
        Potwierdzenie_zaplaty = 54,

        /// <summary>
        /// The superformatka
        /// </summary>
        Superformatka = 99
    }

    /// <summary>
    /// The instance class
    /// </summary>
    public class Enums
    {
    }
}