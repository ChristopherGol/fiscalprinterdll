﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterReceipt.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System.Collections.Generic;
    using System.Linq;
    using ElzabException;
    using Model.Invoice;
    using Model.Product;

    /// <summary>
    /// The Part of the class describes operation with receipt.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        /// <summary>
        /// Prints the receipt.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on position receipt.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire receipt</param>
        public override void PrintReceipt(List<Product> listProducts, object messageOnReceipt, int percentageDiscount)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ReceiptBegin());
            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnReceipt, product.Count, 2, product.Unit, product.Price));
                if (product.TypeDiscount != TypeDiscount.None)
                {
                    switch (product.TypeDiscount)
                    {
                        case TypeDiscount.Percentage:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, ((product.ValueDiscount * product.Price) / 100) * (product.Count / 100)));
                            break;
                        case TypeDiscount.Cost:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, product.ValueDiscount * (product.Count / 100)));
                            break;
                    }
                }
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt with additional text after bill logo.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on position receipt.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire receipt.</param>
        /// <param name="additionalOnReceipt">The additional on receipt after bill logo.</param>
        /// <param name="additionalTextOnReceipt">The additional text on receipt after bill logo.</param>
        public void PrintReceipt(List<Product> listProducts, object messageOnReceipt, int percentageDiscount, object additionalOnReceipt, string additionalTextOnReceipt)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ReceiptBegin());
            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnReceipt, product.Count, 2, product.Unit, product.Price));
                if (product.TypeDiscount != TypeDiscount.None)
                {
                    switch (product.TypeDiscount)
                    {
                        case TypeDiscount.Percentage:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, ((product.ValueDiscount * product.Price) / 100) * (product.Count / 100)));
                            break;
                        case TypeDiscount.Cost:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, product.ValueDiscount * (product.Count / 100)));
                            break;
                    }
                }
            }

            unsafe
            {
                int freeLines;
                pFillLines((int)additionalOnReceipt, additionalTextOnReceipt, &freeLines);
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt with dictionary additional texts after bill logo.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on position receipt.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire receipt.</param>
        /// <param name="additionalsOnReceipt">The dictionary additional texts after bill logo.</param>
        public void PrintReceipt(List<Product> listProducts, object messageOnReceipt, int percentageDiscount, Dictionary<object, string> additionalsOnReceipt)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ReceiptBegin());
            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnReceipt, product.Count, 2, product.Unit, product.Price));
                if (product.TypeDiscount != TypeDiscount.None)
                {
                    switch (product.TypeDiscount)
                    {
                        case TypeDiscount.Percentage:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, ((product.ValueDiscount * product.Price) / 100) * (product.Count / 100)));
                            break;
                        case TypeDiscount.Cost:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, product.ValueDiscount * (product.Count / 100)));
                            break;
                    }
                }
            }

            unsafe
            {
                for (int count = 0; count < additionalsOnReceipt.Count; count++)
                {
                    int freeLines;
                    pFillLines((int)additionalsOnReceipt.Keys.ElementAt(count), additionalsOnReceipt.ElementAt(count).Value, &freeLines);
                }
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt with discount quick response code.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on position receipt.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire receipt.</param>
        /// <param name="discountCode">The discount code prints prints as quick response code (Non-fiscal print).</param>
        public void PrintReceiptWithDiscountQrCode(List<Product> listProducts, object messageOnReceipt, int percentageDiscount, string discountCode)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ReceiptBegin());
            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnReceipt, product.Count, 2, product.Unit, product.Price));
                if (product.TypeDiscount != TypeDiscount.None)
                {
                    switch (product.TypeDiscount)
                    {
                        case TypeDiscount.Percentage:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, ((product.ValueDiscount * product.Price) / 100) * (product.Count / 100)));
                            break;
                        case TypeDiscount.Cost:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, product.ValueDiscount * (product.Count / 100)));
                            break;
                    }
                }
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);

            PrintQrCodeWithDiscount("Kod rabatowy: " + discountCode);

            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt with discount barcode.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on position receipt.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire receipt.</param>
        /// <param name="discountCode">The discount code prints as barcode (Non-fiscal print).</param>
        public void PrintReceiptWithDiscountBarCode(List<Product> listProducts, object messageOnReceipt, int percentageDiscount, string discountCode)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(ReceiptBegin());
            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnReceipt, product.Count, 2, product.Unit, product.Price));
                if (product.TypeDiscount != TypeDiscount.None)
                {
                    switch (product.TypeDiscount)
                    {
                        case TypeDiscount.Percentage:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, ((product.ValueDiscount * product.Price) / 100) * (product.Count / 100)));
                            break;
                        case TypeDiscount.Cost:
                            VerifyImportedFunction.CheckForExceptions(DiscountOrSurcharge(1, product.ValueDiscount * (product.Count / 100)));
                            break;
                    }
                }
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);

            PrintBarCodeWithDiscount(discountCode);

            CommunicationEnd();
        }

        /// <summary>
        /// Prints the invoice.
        /// </summary>
        /// <param name="company">The company information.</param>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnInvoice">The message on position invoice.</param>
        /// <param name="percentageDiscount">The percentage discount for the entire invoice</param>
        public override void PrintInvoice(string numberInvoice, Company company, List<Product> listProducts, object messageOnInvoice, int percentageDiscount)
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(pInvoiceBegin(numberInvoice, company.CompanyName, company.Street + " " + company.NumberBuilding + " " + company.NumberPlace, company.PostalCode + " " + company.City, company.Www, company.Email, company.NumberBankAccount, company.NumberNip));

            foreach (Product product in listProducts)
            {
                VerifyImportedFunction.CheckForExceptions(pReceiptItemEx(1, product.Name, (int)product.VatRate, (int)messageOnInvoice, product.Count, 2, product.Unit, product.Price));
            }

            VerifyImportedFunction.CheckForExceptions(ReceiptEnd(percentageDiscount));
            OpenDrawer(1);
            CommunicationEnd();
        }

        /// <summary>
        /// Open the drawer.
        /// </summary>
        public override void DrawerOpen()
        {
            CommunicationInit(Port, Speed, Timeout);
            VerifyImportedFunction.CheckForExceptions(OpenDrawer(1));
            CommunicationEnd();
        }
    }
}
