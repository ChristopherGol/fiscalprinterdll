﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace FiscalPrinterDll.Elzab.Tests
{
    [TestFixture]
    [Author("Krzysztof Goljasz")]
    internal class TestsWithDevice
    {

        private ElzabFiscalPrinter efp = new ElzabFiscalPrinter();

        [Test]
        [SetUp]
        [Category("Connection")]
        public void TestSearchOpenComPort()
        {
            efp.SearchOpenComPort();

            Assert.AreEqual(3, efp.Port);
        }

        [Test]
        [Category("Information")]
        public void TestUniqueNumber()
        {
            efp.GetUniqueNumberDevice();

            Assert.IsNotEmpty(efp.UniqueNumber);
        }

        [Test]
        [Category("Information")]
        public void TestDeviceName()
        {
            efp.GetDeviceName();

            Assert.AreEqual("ZETA", efp.DeviceName);
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceiptWithoutDiscount()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 200, 265));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35));

            Assert.DoesNotThrow(delegate { efp.PrintReceipt(listProducts, MessageReceipt.BrakKomunikatu, 0); });
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceiptWithCostDiscounts()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 200, 265, TypeDiscount.Cost, 20));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35, TypeDiscount.Cost, 10));

            Assert.DoesNotThrow(delegate { efp.PrintReceipt(listProducts, MessageReceipt.Przecena, 10); });
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceiptWithPercentageDiscounts()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 200, 265, TypeDiscount.Percentage, 20));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35, TypeDiscount.Percentage, 20));

            Assert.DoesNotThrow(delegate { efp.PrintReceipt(listProducts, MessageReceipt.Przecena, 15); });
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceiptWithAdditionalLines()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 200, 265, TypeDiscount.Percentage, 20));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35, TypeDiscount.Percentage, 20));

            Assert.DoesNotThrow(delegate { efp.PrintReceipt(listProducts, MessageReceipt.Przecena, 15, AdditionalLinesReceipt.Obslugujacy, "Krzysztof Goljasz"); });
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceiptWithListAdditionalLines()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 200, 265, TypeDiscount.Percentage, 20));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35, TypeDiscount.Percentage, 20));

            Dictionary<object, string> additionalLines = new Dictionary<object, string>();
            additionalLines.Add(AdditionalLinesReceipt.Obslugujacy, "Krzysztof Goljasz");
            additionalLines.Add(AdditionalLinesReceipt.Zapraszamy, "PONOWNIE!");
            additionalLines.Add(AdditionalLinesReceipt.Numer, "PA 256 01/04/2017");

            Assert.DoesNotThrow(delegate { efp.PrintReceipt(listProducts, MessageReceipt.Przecena, 15, additionalLines); });
        }

        [Test]
        [Category("Invoice")]
        public void TestPrintInvoice()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 100, 265));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35));

            Model.Invoice.Company company = new Model.Invoice.Company("Testowa Sp. z o.o.", "Kolejowa", "6", "lok. 7", "Warszawa", "22-045", "9562364578", "testowa.com", "biuro@testowa.com", "0000000000000000000");

            Assert.DoesNotThrow(delegate { efp.PrintInvoice("65/04/2017",company, listProducts, MessageReceipt.BrakKomunikatu, 10); });

        }

        [Test]
        [Category("Drawer")]
        public void TestDrawerOpen()
        {
            Assert.DoesNotThrow(delegate { efp.DrawerOpen(); });
        }

        [Test]
        [Category("Report")]
        public void TestDayReportWithSale()
        {
            Assert.DoesNotThrow(delegate { efp.DayReport(IfSale.Yes); });
        }

        [Test]
        [Category("Report")]
        public void TestDayReportWithoutSale()
        {
            Assert.DoesNotThrow(delegate { efp.DayReport(IfSale.No); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintLockedProducts()
        {
            Assert.DoesNotThrow(delegate { efp.PrintLockedProduct(); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintStatusDevice()
        {
            Assert.DoesNotThrow(delegate { efp.PrintStatusDevice(); });
        }

        [Test]
        [Category("Report")]
        public void TestMonthReport()
        {
            Assert.DoesNotThrow(delegate { efp.MonthReport(ReportFiscalType.Szczegolowy, new DateTime(2017, 4, 1)); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintPeriodReport()
        {
            Assert.DoesNotThrow(delegate { efp.PeriodReport(ReportFiscalType.Szczegolowy, new DateTime(2017, 4, 1), new DateTime(2017, 4, 1)); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintNumberReport()
        {
            Assert.DoesNotThrow(delegate { efp.NumberReport(ReportFiscalType.Szczegolowy, 1, 3); });
        }

        [Test]
        [Category("Information")]
        public void TestLastReceiptNumber()
        {
            Assert.IsNotNull(efp.GetLastReceiptNumber());
            Assert.That(efp.GetLastReceiptNumber() >= 0);
        }

        [Test]
        [Category("Information")]
        public void TestLastDayReportNumber()
        {
            Assert.IsNotNull(efp.GetLastDayReportNumber());
            Assert.That(efp.GetLastDayReportNumber() > 0);
        }

        [Test]
        [Category("Setting")]
        public void TestGetDeviceTime()
        {
            DateTime deviceTime = efp.GetDeviceTime();

            Assert.IsInstanceOf(typeof(DateTime), deviceTime);
            Assert.That(deviceTime.Hour >= 0 && deviceTime.Minute >= 0 && deviceTime.Second >= 0);
        }

        [Test]
        [Category("Setting")]
        public void TestSetDeviceTime()
        {
            Assert.DoesNotThrow(delegate { efp.ChangeTime(new DateTime(2017, 4, 3, 11, 32, 0)); });
        }

        [Test]
        [Category("Setting")]
        public void TestReadVatFromDevice()
        {
            Assert.IsNotNull(efp.ReadVatFromDevice());
        }

        [Test]
        [Category("Library")]
        public void TestGetElzabLibraryVersion()
        {
            Assert.IsNotEmpty(efp.GetElzabLibraryVersion());
        }

        [Test]
        [Category("Library")]
        public void TestGetElzabLibraryAuthor()
        {
            Assert.IsNotEmpty(efp.GetElzabLibraryAuthor());
        }

        [Test]
        [Category("Setting")]
        public void TestGetDeviceNameVersion()
        {
            efp.GetDeviceNameVersion();

            Assert.That(efp.DeviceName != string.Empty && efp.DeviceAppVersion != string.Empty);

        }

        [Test]
        [Category("OwnFunction")]
        public void TestTurnOffFiscalPrinter()
        {
            Assert.DoesNotThrow(delegate { efp.TurnOffFiscalPrinter(); });
        }

        [Test]
        [Category("OwnFunction")]
        public void TestOwnRSSequences()
        {
            byte[] inputBuffer = new byte[12];
            byte[] outputBuffer = new byte[0];

            Assert.DoesNotThrow(delegate { efp.OwnRSSequences(0x71, 12, 0, inputBuffer, outputBuffer); });
        }
    }
}
