﻿using System;
using System.Collections.Generic;
using FiscalPrinterDll.Elzab.ElzabException;
using NUnit.Framework;

namespace FiscalPrinterDll.Elzab.Tests
{
    [TestFixture]
    [Author("Krzysztof Goljasz")]
    class TestsWithoutDevice
    {
        private ElzabFiscalPrinter efp = new ElzabFiscalPrinter();

        [Test]
        [SetUp]
        [Category("Connection")]
        public void TestSearchOpenComPort()
        {
            efp.SearchOpenComPort();

            Assert.IsTrue(efp.Port == 0);
        }

        [Test]
        [Category("Information")]
        public void TestUniqueNumber()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetUniqueNumberDevice(); });
            Assert.IsNull(efp.UniqueNumber);
        }

        [Test]
        [Category("Information")]
        public void TestDeviceName()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetDeviceName(); });
            Assert.IsNull(efp.DeviceName);
        }

        [Test]
        [Category("Receipt")]
        public void TestPrintReceipt()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 100, 265));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35));

            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.PrintReceipt(listProducts, MessageReceipt.Przecena, 0); });
        }

        [Test]
        [Category("Invoice")]
        public void TestPrintInvoice()
        {
            List<Model.Product.Product> listProducts = new List<Model.Product.Product>();
            listProducts.Add(new Model.Product.Product("Chleb zwykły", VatRate.A, "szt.", 100, 265));
            listProducts.Add(new Model.Product.Product("Bułki codzienne", VatRate.B, "szt.", 600, 35));

            Model.Invoice.Company company = new Model.Invoice.Company("Testowa Sp. z o.o.", "Kolejowa", "6", "lok. 7", "Warszawa", "22-045", "9562364578", "testowa.com", "biuro@testowa.com", "0000000000000000000");

            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.PrintInvoice("65/04/2017",company, listProducts, MessageReceipt.BrakKomunikatu, 10); });
        }

        [Test]
        [Category("Drawer")]
        public void TestDrawerOpen()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.DrawerOpen(); });
        }

        [Test]
        [Category("Report")]
        public void TestDayReportWithSale()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.DayReport(IfSale.Yes); });
        }

        [Test]
        [Category("Report")]
        public void TestDayReportWithoutSale()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.DayReport(IfSale.No); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintLockedProducts()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.PrintLockedProduct(); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintStatusDevice()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.PrintStatusDevice(); });
        }

        [Test]
        [Category("Report")]
        public void TestMonthReport()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.MonthReport(ReportFiscalType.Szczegolowy, new DateTime(2017, 4, 1)); });
        }

        [Test]
        [Category("Report")]
        public void TestPrintPeriodReport()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.PeriodReport(ReportFiscalType.Szczegolowy, new DateTime(2017, 4, 1), new DateTime(2017, 4, 1)); });

        }

        [Test]
        [Category("Report")]
        public void TestPrintNumberReport()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.NumberReport(ReportFiscalType.Szczegolowy, 1, 3); });
        }

        [Test]
        [Category("Information")]
        public void TestLastReceiptNumber()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetLastReceiptNumber(); });
        }

        [Test]
        [Category("Information")]
        public void TestLastDayReportNumber()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetLastDayReportNumber(); });
        }

        [Test]
        [Category("Setting")]
        public void TestGetDeviceTime()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetDeviceTime(); });
        }

        [Test]
        [Category("Setting")]
        public void TestSetDeviceTime()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.ChangeTime(new DateTime(2017, 4, 3, 15, 48, 0)); });
        }

        [Test]
        [Category("Setting")]
        public void TestReadVatFromDevice()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.ReadVatFromDevice(); });
        }

        [Test]
        [Category("Library")]
        public void TestGetElzabLibraryVersion()
        {
            Assert.IsNotEmpty(efp.GetElzabLibraryVersion());
        }

        [Test]
        [Category("Library")]
        public void TestGetElzabLibraryAuthor()
        {
            Assert.IsNotEmpty(efp.GetElzabLibraryAuthor());
        }

        [Test]
        [Category("Information")]
        public void TestGetDeviceNameVersion()
        {
            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.GetDeviceNameVersion(); });
        }

        [Test]
        [Category("OwnFunction")]
        public void TestOwnRSSequences()
        {
            byte[] inputBuffer = new byte[12];
            byte[] outputBuffer = new byte[0];

            Assert.Throws<ElzabFiscalPrinterException>(delegate { efp.OwnRSSequences(0x71, 12, 0, inputBuffer, outputBuffer); });
        }

    }
}
