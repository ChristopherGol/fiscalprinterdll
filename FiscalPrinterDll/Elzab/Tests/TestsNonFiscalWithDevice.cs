﻿using NUnit.Framework;

namespace FiscalPrinterDll.Elzab.Tests
{
    [TestFixture]
    [Author("Krzysztof Goljasz")]
    internal class TestsNonFiscalWithDevice
    {
        private ElzabFiscalPrinter _efp = new ElzabFiscalPrinter();

        [Test]
        [SetUp]
        [Category("Connection")]
        public void InitializeTest()
        {
            _efp.SearchOpenComPort();

            Assert.That(_efp.Port > 0);
        }

        [Test]
        [Category("StateFiscalPrinter")]
        public void TestPrintStateFiscalPrinter()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintStateFiscalPrinter("Anna Kowalska", "2 dn. 03/04/2017", "0 zł", "1500 zł", "0 zł", "Krzysztof Goljasz", "SK 03/04/2017", "22568", "SK225", "SK225", "156987453698565"); });
        }

        [Test]
        [Category("MoneyInDrawer")]
        public void TestPrintReceiptPayment()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintReceiptPayment("KP 2 04/04/2017", "Krzysztof Goljasz", "KP 2569", "2500 zł", "Krzysztof Goljasz", "KP 2 04/04/2017", "Nr 25264", "", "", "15698756458696"); });
        }

        [Test]
        [Category("MoneyInDrawer")]
        public void TestPrintReceiptPaycheck()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintReceiptPaycheck("KW 3 04/04/2017", "Krzysztof Goljasz", "5600 zł", "Krzysztof Goljasz", "KW 3 04/04/2017", "Nr 56984", "", "", "15575693654789"); });
        }

        [Test]
        [Category("Order")]
        public void TestOrder()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintOrder("Anna Kowalska", "Zam. 56983", "Piz. 24", " Sos 896", "2", "PA 25/03/2017", "Nr 569", "", "", "14596365863254"); });
        }

        [Test]
        [Category("Order")]
        public void TestOrderWaiter()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintOrderWaiter("Paulina Nowak", "Stolik nr 5", "Wino wytrwane 0.33 L", "Francuskie wino wytrawne 0.33 L", "4", "PA 05/04/2017", "Nr 5698", "", "", "145698756356980"); });
        }

        [Test]
        [Category("Phone")]
        public void TestPrintPhoneRecharge()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintPhoneRecharge("MID4545454654", "TID45646461", "DOŁADOWANIE PLUS", "569645636854", "+48 123 456 789", "656456456465465456456465", "01/05/2017", "+48 123 456 789", "14569875896536"); });
        }
        
        [Test]
        [Category("QRCode")]
        public void TestPrintQrCode()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintQrCode("Kod rabatowy na zakupy w sklepie XXXXX SP. Z O.O.: 4f6d6fd6df6sdfds6fds6fds6"); });
        }

        [Test]
        [Category("Coupon")]
        public void TestPrintDiscountCoupon()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintDiscountCoupon("50 zł", "10 lat XXXXX", "Krzysztof Goljasz", "XXXXX Sp. z o.o.", "Krzysztof Goljasz", "Nr. 5698", "Nr. 56494946", "", "", "5965874563256985"); });
        }

        [Test]
        public void TestPrintCarWashTicket()
        {
            Assert.DoesNotThrow(delegate { _efp.PrintCarWashTicket("01", "02", "CAR02WASHx8", "CW 569/04/2017", "NR. 2536987", "", "", "15469875632596"); });
        }

    }
}
