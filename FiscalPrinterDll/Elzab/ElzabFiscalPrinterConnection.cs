﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterConnection.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System;
    using ElzabException;

    /// <summary>
    /// The Part of the class describes the connection to the fiscal printer.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        /// <summary>
        /// It optimizes the communication to the fiscal printer. It sends less data to device which can speed up the work of the remote desktop. 
        /// </summary>
        public void OptimizeCommunication()
        {
            VerifyImportedFunction.CheckForExceptions(OptimizeTransmission());
        }

        /// <summary>
        /// Searches the open COM port for the fiscal printer.
        /// </summary>
        public override void SearchOpenComPort()
        {
            int tmpPort = 0;

            while (tmpPort < 255)
            {
                if (CommunicationInit(tmpPort, Speed, Timeout) == 0)
                {
                    break;
                }

                tmpPort++;
            }

            Port = tmpPort;
        }

        /// <summary>
        /// Open the connection with the fiscal device over the network.
        /// </summary>
        public override void OpenIPConnection()
        {
            CommunicationInit2(IP, TcpPort, Timeout);
        }

    }
}
