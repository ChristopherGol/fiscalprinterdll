﻿//-----------------------------------------------------------------------
// <copyright file="VerifyImportedFunction.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab.ElzabException
{
    /// <summary>
    /// The class describes method that allows for verify imported function from C++
    /// </summary>
    internal class VerifyImportedFunction
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="VerifyImportedFunction"/> class from being created.
        /// </summary>
        private VerifyImportedFunction()
        {
        }

        /// <summary>
        /// Checks for exceptions.
        /// </summary>
        /// <param name="resultMethod">The result method.</param>
        /// <exception cref="FiscalPrinterDll.Elzab.ElzabException.ElzabFiscalPrinterException">
        /// return ElzabFiscalPrinterException instance
        /// </exception>
        public static void CheckForExceptions(int resultMethod)
        {
            if (resultMethod != 0)
            {
                throw new ElzabFiscalPrinterException(resultMethod);
            }
        }
    }
}
