﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterException.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab.ElzabException
{
    using System;

    /// <summary>
    /// The class describes exceptions which it creates during communication with fiscal printer.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ElzabFiscalPrinterException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinterException"/> class.
        /// </summary>
        public ElzabFiscalPrinterException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinterException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ElzabFiscalPrinterException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinterException"/> class.
        /// </summary>
        /// <param name="numberError">The number error.</param>
        public ElzabFiscalPrinterException(int numberError) : base(GetTextError(numberError))
        {
        }

        /// <summary>
        /// Gets the text error based on results function.
        /// </summary>
        /// <param name="numberError">The number error.</param>
        /// <returns>The message error.</returns>
        private static string GetTextError(int numberError)
        {
            string information;

            switch (numberError)
            {
                case 0:
                    information = "Funkcja zakończona poprawnie.";
                    break;
                case 2:
                    information = "Nie można otworzyć portu szeregowego w systemie. Brak portu lub używany przez inną aplikację.";
                    break;
                case 20:
                    information = "Błąd połączenia drukarki fiskalnej z komputerem. Proszę sprawdzić połączenie.";
                    break;
                case 21:
                    information = "Błąd połączenia drukarki fiskalnej z komputerem. Proszę sprawdzić połączenie i zgodność szybkości transmisji.";
                    break;
                case 23:
                    information = "Błąd połączenia drukarki fiskalnej z komputerem. Proszę sprawdzić połączenie i zgodność szybkości transmisji.";
                    break;
                case 99:
                    information = "Błąd inicjalizacji portu szeregowego.";
                    break;
                case 100:
                    information = "Problem z drukarką wymagający interwencji serwisowej.";
                    break;
                case 101:
                    information = "Proszę wezwać serwis w celu przestawienia zwory serwisowej w pozycję normalną.";
                    break;
                case 102:
                    information = "Awaria kontrolera drukarki fiskalnej. Proszę wezwać serwis.";
                    break;
                case 103:
                    information = "Ogólna awaria drukarki fiskalnej. Proszę wezwać serwis.";
                    break;
                case 104:
                    information = "Należy wprowadzić nagłówek wydruków programem serwisowym.";
                    break;
                case 105:
                    information = "Drukarka w trybie tylko do odczytu. Proszę wezwać serwis.";
                    break;
                case 106:
                    information = "Problem z wyświetlaczem zewnętrznym. Proszę sprawdzić poprawność podłączenia wyświetlacza zewnętrznego.";
                    break;
                case 107:
                    information = "Niezgodność czasu drukarki fiskalnej z komputerem lub czas niepoprawny. Proszę skorygować zegar drukarki programem serwisowym.";
                    break;
                case 108:
                    information = "Należy wykonać zaległy raport dobowy.";
                    break;
                case 109:
                    information = "Brak papieru w drukarce fiskalnej. Proszę wymienić papier na nowy i wznowić wydruk.";
                    break;
                case 110:
                    information = "Proszę zaczekać do końca wydruku na drukarce fiskalnej.";
                    break;
                case 111:
                    information = "Proszę zaczekać do końca wydruku na drukarce fiskalnej.";
                    break;
                case 112:
                    information = "Proszę wznowić wydruk na drukarce fiskalnej.";
                    break;
                case 113:
                    information = "Brak lub niepoprawne stawki VAT na drukarce fiskalnej. Proszę ustawić stawki VAT programem serwisowym.";
                    break;
                case 114:
                    information = "Niepoprawny port szeregowy do komunikacji komputera z drukarką fiskalną. Proszę otworzyć port o właściwym numerze.";
                    break;
                case 200:
                    information = "Operacja niemożliwa do wykonania w trakcie otwartego paragonu na drukarce fiskalnej.";
                    break;
                case 201:
                    information = "Operacja niemożliwa do wykonana poza paragonem na drukarce fiskalnej.";
                    break;
                case 202:
                    information = "Nazwa towaru zbyt krótka. Proszę wydłużyć nazwę danego asortymentu.";
                    break;
                default:
                    information = "Błąd systemowy drukarki fiskalnej. Proszę skontakować się z producentem urządzenia fiskalnego Elzab lub macierzystym serwisem urządzeń.";
                    break;
            }

            return information;
        }
    }
}
