﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterNoFiscalException.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab.ElzabException
{
    using System;

    /// <summary>
    /// The class describes throw exceptions during print non-fiscal printout.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ElzabFiscalPrinterNoFiscalException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinterNoFiscalException"/> class.
        /// </summary>
        public ElzabFiscalPrinterNoFiscalException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElzabFiscalPrinterNoFiscalException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ElzabFiscalPrinterNoFiscalException(string message) : base(message)
        {
        }  
    }
}
