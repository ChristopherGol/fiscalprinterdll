﻿//-----------------------------------------------------------------------
// <copyright file="VerifyArgumentsFunction.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab.ElzabException
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The class describes method allow to control parameters.
    /// </summary>
    internal class VerifyArgumentsFunction
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="VerifyArgumentsFunction"/> class from being created.
        /// </summary>
        private VerifyArgumentsFunction()
        {
        }

        /// <summary>
        /// Checks the method arguments.
        /// </summary>
        /// <param name="listArguments">The list arguments.</param>
        /// <exception cref="FiscalPrinterDll.Elzab.ElzabException.ElzabFiscalPrinterNoFiscalException">
        /// Zbyt duża liczba cyfr w podanym argumencie do metody.
        /// or
        /// Zbyt duża liczba liter w podanym argumencie do metody.
        /// or
        /// Zbyt długi kod kreskowy lub niepoprawne podane wartości do wydruku kodu.
        /// </exception>
        public static void CheckArguments(Dictionary<string, string> listArguments)
        {
            foreach (KeyValuePair<string, string> entry in listArguments)
            {
                if (entry.Key.Contains("L"))
                {
                    if (entry.Value.ToCharArray().Count(c => char.IsDigit(c)) > 3)
                    {
                        throw new ElzabFiscalPrinterNoFiscalException("Zbyt duża liczba cyfr w podanym argumencie do metody.");
                    }
                }
                else if (entry.Key.Contains("C"))
                {
                    if (entry.Value.ToCharArray().Count(c => char.IsLetter(c)) > 3)
                    {
                        throw new ElzabFiscalPrinterNoFiscalException("Zbyt duża liczba liter w podanym argumencie do metody.");
                    }
                }
                else if (entry.Key.Contains("A"))
                {
                }
                else if (entry.Key.Contains("K"))
                {
                    if (entry.Value.Length > 16 || entry.Value.ToCharArray().Count(c => (int)c > 126) > 0)
                    {
                        throw new ElzabFiscalPrinterNoFiscalException("Zbyt długi kod kreskowy lub niepoprawne podane wartości do wydruku kodu.");
                    }
                }
            }
        }
    }
}
