﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterNoFiscal.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System;
    using System.Collections.Generic;
    using ElzabException;

    /// <summary>
    /// The Part of the class describes non-fiscal prints.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        /// <summary>
        /// Prints the state fiscal printer.
        /// </summary>
        /// <param name="cashier">The cashier.</param>
        /// <param name="shift">The shift.</param>
        /// <param name="oweMoney">The owe money.</param>
        /// <param name="countedCash">The counted money.</param>
        /// <param name="differenceCash">The difference money.</param>
        /// <param name="author">The author.</param>
        /// <param name="numberDocument">The number document.</param>
        /// <param name="numberPrintout">The number printout.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintStateFiscalPrinter(string cashier, string shift, string oweMoney, string countedCash, string differenceCash, string author, string numberDocument, string numberPrintout, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "L1", cashier },
                { "C2", shift },
                { "C3", oweMoney },
                { "C4", countedCash },
                { "C5", differenceCash },
                { "L6", author },
                { "C7", numberDocument },
                { "C8", numberPrintout },
                { "L9", code },
                { "L10", number },
                { "K11", barcode }
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(10);

            pNonFiscalPrintoutLine(10, cashier, 1);
            pNonFiscalPrintoutLine(20, shift, 1);
            pNonFiscalPrintoutLine(30, oweMoney, 1);
            pNonFiscalPrintoutLine(40, countedCash, 1);
            pNonFiscalPrintoutLine(50, string.Empty, 1);
            pNonFiscalPrintoutLine(60, differenceCash, 1);
            pNonFiscalPrintoutLine(70, author, 1);
            pNonFiscalPrintoutLine(80, string.Empty, 1);
            pNonFiscalPrintoutLine(90, numberDocument, 1);
            pNonFiscalPrintoutLine(01, numberPrintout, 1);
            pNonFiscalPrintoutLine(11, code, 1);
            pNonFiscalPrintoutLine(21, number, 1);
            pNonFiscalPrintoutLine(31, barcode, 1);

            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt payment.
        /// </summary>
        /// <param name="numberReceipt">The number receipt.</param>
        /// <param name="payer">The payer.</param>
        /// <param name="titlePayment">The title payment.</param>
        /// <param name="paid">The paid.</param>
        /// <param name="amountReceived">The amount received.</param>
        /// <param name="documentNumber">The document number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintReceiptPayment(string numberReceipt, string payer, string titlePayment, string paid, string amountReceived, string documentNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "C1", numberReceipt },
                { "L2", payer },
                { "C3", titlePayment },
                { "C4", paid },
                { "L5", amountReceived },
                { "C6", documentNumber },
                { "C7", printoutNumber },
                { "L8", code },
                { "L9", number },
                { "K10", barcode }
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(20);
            pNonFiscalPrintoutLine(10, numberReceipt, 1);
            pNonFiscalPrintoutLine(20, payer, 1);
            pNonFiscalPrintoutLine(30, titlePayment, 1);
            pNonFiscalPrintoutLine(40, paid, 1);
            pNonFiscalPrintoutLine(50, string.Empty, 1);
            pNonFiscalPrintoutLine(60, amountReceived, 1);
            pNonFiscalPrintoutLine(70, string.Empty, 1);
            pNonFiscalPrintoutLine(80, documentNumber, 1);
            pNonFiscalPrintoutLine(90, printoutNumber, 1);
            pNonFiscalPrintoutLine(01, code, 1);
            pNonFiscalPrintoutLine(11, number, 1);
            pNonFiscalPrintoutLine(21, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the receipt paycheck.
        /// </summary>
        /// <param name="numberReceipt">The number receipt.</param>
        /// <param name="payer">The payer.</param>
        /// <param name="paid">The paid.</param>
        /// <param name="personReceivedMoney">The person received money.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintReceiptPaycheck(string numberReceipt, string payer, string paid, string personReceivedMoney, string receiptNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "C1", numberReceipt },
                { "L2", payer },
                { "C3", paid },
                { "L4", personReceivedMoney },
                { "C5", receiptNumber },
                { "C6", printoutNumber },
                { "L7", code },
                { "L8", number },
                { "K9", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(30);
            pNonFiscalPrintoutLine(10, numberReceipt, 1);
            pNonFiscalPrintoutLine(20, payer, 1);
            pNonFiscalPrintoutLine(40, paid, 1);
            pNonFiscalPrintoutLine(50, string.Empty, 1);
            pNonFiscalPrintoutLine(60, personReceivedMoney, 1);
            pNonFiscalPrintoutLine(70, string.Empty, 1);
            pNonFiscalPrintoutLine(80, receiptNumber, 1);
            pNonFiscalPrintoutLine(90, printoutNumber, 1);
            pNonFiscalPrintoutLine(01, code, 1);
            pNonFiscalPrintoutLine(11, number, 1);
            pNonFiscalPrintoutLine(21, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the order.
        /// </summary>
        /// <param name="purchaser">The purchaser.</param>
        /// <param name="title">The title order.</param>
        /// <param name="name">The name product.</param>
        /// <param name="modifier">The modifier product.</param>
        /// <param name="count">The count product.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintOrder(string purchaser, string title, string name, string modifier, string count, string receiptNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "L1", purchaser },
                { "C2", title },
                { "C3", name },
                { "C4", modifier },
                { "C5", count },
                { "C6", receiptNumber },
                { "C7", printoutNumber },
                { "L8", code },
                { "L9", number },
                { "K10", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(70);
            pNonFiscalPrintoutLine(10, purchaser, 1);
            pNonFiscalPrintoutLine(20, title, 1);
            pNonFiscalPrintoutLine(30, string.Empty, 1);
            pNonFiscalPrintoutLine(40, title, 1);
            pNonFiscalPrintoutLine(50, modifier, 1);
            pNonFiscalPrintoutLine(60, count, 1);
            pNonFiscalPrintoutLine(70, string.Empty, 1);
            pNonFiscalPrintoutLine(80, string.Empty, 1);
            pNonFiscalPrintoutLine(90, string.Empty, 1);
            pNonFiscalPrintoutLine(01, receiptNumber, 1);
            pNonFiscalPrintoutLine(11, printoutNumber, 1);
            pNonFiscalPrintoutLine(21, code, 1);
            pNonFiscalPrintoutLine(31, number, 1);
            pNonFiscalPrintoutLine(41, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the order waiter.
        /// </summary>
        /// <param name="waiter">The waiter.</param>
        /// <param name="table">The table.</param>
        /// <param name="product">The product.</param>
        /// <param name="description">The description.</param>
        /// <param name="count">The count product.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintOrderWaiter(string waiter, string table, string product, string description, string count, string receiptNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "A1", waiter },
                { "A2", table },
                { "A3", product },
                { "A4", description },
                { "C5", count },
                { "C6", receiptNumber },
                { "C7", printoutNumber },
                { "L8", code },
                { "L9", number },
                { "K10", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(53);
            pNonFiscalPrintoutLine(10, string.Empty, 1);
            pNonFiscalPrintoutLine(20, waiter, 1);
            pNonFiscalPrintoutLine(30, table, 1);
            pNonFiscalPrintoutLine(40, product, 1);
            pNonFiscalPrintoutLine(50, description, 1);
            pNonFiscalPrintoutLine(70, count, 1);
            pNonFiscalPrintoutLine(80, receiptNumber, 1);
            pNonFiscalPrintoutLine(90, printoutNumber, 1);
            pNonFiscalPrintoutLine(01, code, 1);
            pNonFiscalPrintoutLine(11, number, 1);
            pNonFiscalPrintoutLine(21, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the discount coupon.
        /// </summary>
        /// <param name="valueDiscount">The value discount.</param>
        /// <param name="name">The name of the discount.</param>
        /// <param name="cashier">The cashier.</param>
        /// <param name="shopName">Name of the shop.</param>
        /// <param name="author">The author.</param>
        /// <param name="documentNumber">The document number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintDiscountCoupon(string valueDiscount, string name, string cashier, string shopName, string author, string documentNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "C1", valueDiscount },
                { "L2", name },
                { "L3", cashier },
                { "L6", shopName },
                { "L7", author },
                { "C8", documentNumber },
                { "C9", printoutNumber },
                { "L10", code },
                { "L1", number },
                { "K12", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(71);
            pNonFiscalPrintoutLine(20, string.Empty, 1);
            pNonFiscalPrintoutLine(40, string.Empty, 1);
            pNonFiscalPrintoutLine(80, valueDiscount, 1);
            pNonFiscalPrintoutLine(01, name, 1);
            pNonFiscalPrintoutLine(11, cashier, 1);
            pNonFiscalPrintoutLine(21, DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, 1);
            pNonFiscalPrintoutLine(31, DateTime.Now.Hour + ":" + DateTime.Now.Minute, 1);
            pNonFiscalPrintoutLine(41, shopName, 1);
            pNonFiscalPrintoutLine(51, author, 1);
            pNonFiscalPrintoutLine(61, string.Empty, 1);
            pNonFiscalPrintoutLine(71, documentNumber, 1);
            pNonFiscalPrintoutLine(81, printoutNumber, 1);
            pNonFiscalPrintoutLine(91, code, 1);
            pNonFiscalPrintoutLine(02, number, 1);
            pNonFiscalPrintoutLine(12, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the phone recharge.
        /// </summary>
        /// <param name="mid">The machine identification number.</param>
        /// <param name="tid">The taxpayer identification number.</param>
        /// <param name="title">The title.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="codeRecharge">The code recharge.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="phoneNumberSupport">The phone number support.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintPhoneRecharge(string mid, string tid, string title, string cardNumber, string phoneNumber, string codeRecharge, string expirationDate, string phoneNumberSupport, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "C1", mid },
                { "C2", tid },
                { "L3", title },
                { "C4", cardNumber },
                { "C5", phoneNumber },
                { "C6", codeRecharge },
                { "C7", expirationDate },
                { "C8", phoneNumberSupport },
                { "K9", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(23);
            pNonFiscalPrintoutLine(20, string.Empty, 1);
            pNonFiscalPrintoutLine(01, DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, 1);
            pNonFiscalPrintoutLine(11, DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second, 1);
            pNonFiscalPrintoutLine(21, mid, 1);
            pNonFiscalPrintoutLine(31, tid, 1);
            pNonFiscalPrintoutLine(81, title, 1);
            pNonFiscalPrintoutLine(91, cardNumber, 1);
            pNonFiscalPrintoutLine(02, phoneNumber, 1);
            pNonFiscalPrintoutLine(12, codeRecharge, 1);
            pNonFiscalPrintoutLine(33, expirationDate, 1);
            pNonFiscalPrintoutLine(53, string.Empty, 1);
            pNonFiscalPrintoutLine(63, phoneNumberSupport, 1);
            pNonFiscalPrintoutLine(73, string.Empty, 1);
            pNonFiscalPrintoutLine(83, string.Empty, 1);
            pNonFiscalPrintoutLine(34, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the car wash ticket.
        /// </summary>
        /// <param name="valid">Validity period of the ticket.</param>
        /// <param name="programNumber">The program number.</param>
        /// <param name="codeWash">The code wash.</param>
        /// <param name="documentNumber">The document number.</param>
        /// <param name="printoutNumber">The printout number.</param>
        /// <param name="code">The code.</param>
        /// <param name="number">The number.</param>
        /// <param name="barcode">The barcode.</param>
        public void PrintCarWashTicket(string valid, string programNumber, string codeWash, string documentNumber, string printoutNumber, string code, string number, string barcode)
        {
            Dictionary<string, string> listArguments = new Dictionary<string, string>()
            {
                { "C1", valid },
                { "C2", programNumber },
                { "L3", codeWash },
                { "C4", documentNumber },
                { "C5", printoutNumber },
                { "L6", code },
                { "L7", number },
                { "K8", barcode },
            };

            VerifyArgumentsFunction.CheckArguments(listArguments);

            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(92);
            pNonFiscalPrintoutLine(10, valid, 1);
            pNonFiscalPrintoutLine(20, programNumber, 1);
            pNonFiscalPrintoutLine(30, string.Empty, 1);
            pNonFiscalPrintoutLine(50, codeWash, 1);
            pNonFiscalPrintoutLine(60, DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, 1);
            pNonFiscalPrintoutLine(70, DateTime.Now.Hour + ":" + DateTime.Now.Minute, 1);
            pNonFiscalPrintoutLine(80, documentNumber, 1);
            pNonFiscalPrintoutLine(90, printoutNumber, 1);
            pNonFiscalPrintoutLine(02, code, 1);
            pNonFiscalPrintoutLine(12, number, 1);
            pNonFiscalPrintoutLine(22, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the quick response code with discount.
        /// </summary>
        /// <param name="qrcode">The quick response code.</param>
        public void PrintQrCodeWithDiscount(string qrcode)
        {
            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(99);
            pNonFiscalPrintoutLine(10, "Kod rabatowy do wykorzystania:", 1);
            pNonFiscalPrintoutLine(01, qrcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the bar code with discount.
        /// </summary>
        /// <param name="barcode">The barcode.</param>
        public void PrintBarCodeWithDiscount(string barcode)
        {
            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(99);
            pNonFiscalPrintoutLine(10, "Kod rabatowy do wykorzystania:", 1);
            pNonFiscalPrintoutLine(90, barcode, 1);
            pNonFiscalPrintoutLine(10, barcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }

        /// <summary>
        /// Prints the quick response code.
        /// </summary>
        /// <param name="qrcode">The quick response code.</param>
        public void PrintQrCode(string qrcode)
        {
            CommunicationInit(Port, Speed, Timeout);
            NonFiscalPrintoutBegin(99);
            pNonFiscalPrintoutLine(01, qrcode, 1);
            NonFiscalPrintoutEnd();
            CommunicationEnd();
        }
    }
}
