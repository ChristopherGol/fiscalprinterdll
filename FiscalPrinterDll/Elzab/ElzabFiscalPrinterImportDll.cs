﻿//-----------------------------------------------------------------------
// <copyright file="ElzabFiscalPrinterImportDll.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Elzab
{
    using System.Runtime.InteropServices;
    using System.Text;

    /// <summary>
    /// Describes import function from C++ library.
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.FiscalPrinter" />
    public partial class ElzabFiscalPrinter
    {
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CommunicationInit(int portNumber, int speed, int timeout);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CommunicationInit2(string IP, int Port, int Timeout);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int CommunicationEnd();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int OptimizeTransmission();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReceiptConditions();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReceiptBegin();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pReceiptItemEx(int typeSale, string name, int vatRate, int messageOnReceipt, int count, int accuracy, string unit, int price);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int pReceiptItem(int typeSale, string name, int vatRate, int messageOnReceipt, int count, int accuracy, string unit, int price, int* value);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int DiscountOrSurcharge(int typeDiscount, int cost);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PercentDiscountOrSurcharge(int typeDiscount, int percentage);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReceiptEnd(int discount);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PrintSubtotal();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int OpenDrawer(int number);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ReceiptCancel();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PrintControl(int beforePrinting);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PrintResume();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int DailyReport(int unconditionally);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PeriodReport(int fiscal, int yearFrom, int monthFrom, int dayFrom, int yearTo, int monthTo, int dayTo);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int LockedArticlesReport();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int MonthlyReport(int fiscal, int year, int month);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int NumberReport(int fiscal, int firstNumber, int lastNumber);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int ReceiptNumber(int* number);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int DailyReportNumber(int* number);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int WriteLineFeed(int number);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PrinterStatusReport();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int SetVAT(int* count, int a, int b, int c, int d, int e, int f, int g);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int ReadVAT(int* count, int* a, int* b, int* c, int* d, int* e, int* f, int* g);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ErasePayments();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int EraseLines();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pDeviceName(StringBuilder name);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pFillPayment(int numberPayment, string namePayment, int total, int rest);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int pFillLines(int numberMessage, string lineAdditionText, int* freeLines);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pDllVersion(StringBuilder version);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pDllAuthor(StringBuilder author);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pErrMessage(int numberError, StringBuilder messageError);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PCharsInArticleName(string name);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pReadClock(StringBuilder timeDevice);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pReadUniqueNumber(StringBuilder uniqueNumber);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pReadTotal(StringBuilder total);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pReadSelTotal(string rate, StringBuilder total);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int pDisplayFP600(int number, char* caption);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ChangeTime(int hour, int minute);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int PackageItem(int typePackage, int numberPackage, int quantity, int price);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pControlPrintout(int operation, string name, int vatRate);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int SetDebugMode();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int ClearDebugMode();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pWriteFooterLines(string lineOne, string lineTwo, string lineThree);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pInvoiceBegin(string numberInvoice, string lineOne, string lineTwo, string lineThree, string lineFour, string lineFive, string lineSix, string nip);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern unsafe int DeviceType(Device* deviceType, int* deviceVersion);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int NonFiscalPrintoutBegin(int numer);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int NonFiscalPrintoutEnd();
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pNonFiscalPrintoutLine(int numberLine, string text, int if0A);
        [DllImport("elzabdr.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int RSSequence(int controlCode, int quantityOfBytesToReceive, int quantityOfBytesToSend, byte[] inputBuffer, byte[] outputBuffer);
    }
}
