﻿//-----------------------------------------------------------------------
// <copyright file="Company.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Model.Invoice
{
    /// <summary>
    /// Company class describes information about company
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Company"/> class.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="street">The street.</param>
        /// <param name="numberBuilding">The number building.</param>
        /// <param name="numberPlace">The number place.</param>
        /// <param name="city">The city.</param>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="numberNip">The number nip.</param>
        /// <param name="www">The WWW.</param>
        /// <param name="email">The email.</param>
        /// <param name="numberBankAccount">The number bank account.</param>
        public Company(string companyName, string street, string numberBuilding, string numberPlace, string city, string postalCode, string numberNip, string www, string email, string numberBankAccount)
        {
            CompanyName = companyName;
            Street = street;
            NumberBuilding = numberBuilding;
            NumberPlace = numberPlace;
            City = city;
            PostalCode = postalCode;
            NumberNip = numberNip;
            Www = www;
            Email = email;
            NumberBankAccount = numberBankAccount;
        }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>
        /// The street.
        /// </value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the number building.
        /// </summary>
        /// <value>
        /// The number building.
        /// </value>
        public string NumberBuilding { get; set; }

        /// <summary>
        /// Gets or sets the number place.
        /// </summary>
        /// <value>
        /// The number place.
        /// </value>
        public string NumberPlace { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the number NIP.
        /// </summary>
        /// <value>
        /// The number nip.
        /// </value>
        public string NumberNip { get; set; }

        /// <summary>
        /// Gets or sets the website www.
        /// </summary>
        /// <value>
        /// The WWW.
        /// </value>
        public string Www { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the bank account number.
        /// </summary>
        /// <value>
        /// The bank account number.
        /// </value>
        public string NumberBankAccount { get; set; }
    }
}
