﻿//-----------------------------------------------------------------------
// <copyright file="Product.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll.Model.Product
{
    using Elzab;

    /// <summary>
    /// Product class describes product information
    /// </summary>
    public class Product
    {
        /// <summary>
        /// The product unit
        /// </summary>
        private string _unit;

        /// <summary>
        /// The product count
        /// </summary>
        private int _count;

        /// <summary>
        /// The product price one piece
        /// </summary>
        private int _price;

        /// <summary>
        /// The value discount (amount or percentage)
        /// </summary>
        private int _valueDiscount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="vatRate">The vat rate.</param>
        /// <param name="unit">The unit.</param>
        /// <param name="count">The count.</param>
        /// <param name="price">The price.</param>
        public Product(string name, VatRate vatRate, string unit, int count, int price)
        {
            Name = name;
            VatRate = vatRate;
            _unit = unit;
            _count = count;
            _price = price;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="vatRate">The vat rate.</param>
        /// <param name="unit">The unit.</param>
        /// <param name="count">The count.</param>
        /// <param name="price">The price.</param>
        /// <param name="typeDiscount">The type discount.</param>
        /// <param name="valueDiscount">The value discount.</param>
        public Product(string name, VatRate vatRate, string unit, int count, int price, TypeDiscount typeDiscount, int valueDiscount) : this(name, vatRate, unit, count, price)
        {
            TypeDiscount = typeDiscount;
            _valueDiscount = valueDiscount;
        }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        /// <value>
        /// The product name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the product vat rate.
        /// </summary>
        /// <value>
        /// The product vat rate.
        /// </value>
        public VatRate VatRate { get; set; }

        /// <summary>
        /// Gets or sets the product unit.
        /// </summary>
        /// <value>
        /// The product unit.
        /// </value>
        public string Unit
        {
            get
            {
                return _unit;
            }

            set
            {
                if (value.Length <= 4)
                {
                    _unit = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the product count.
        /// </summary>
        /// <value>
        /// The product count.
        /// </value>
        public int Count
        {
            get
            {
                return _count;
            }

            set
            {
                if (value > 0 && value < 999999)
                {
                    _count = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the product type discount.
        /// </summary>
        /// <value>
        /// The product type discount.
        /// </value>
        public TypeDiscount TypeDiscount { get; set; } = TypeDiscount.None;

        /// <summary>
        /// Gets or sets the product value discount.
        /// </summary>
        /// <value>
        /// The product value discount.
        /// </value>
        public int ValueDiscount
        {
            get
            {
                return _valueDiscount;
            }

            set
            {
                if (value > 0 && value < _price)
                {
                    _valueDiscount = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the product price.
        /// </summary>
        /// <value>
        /// The product price.
        /// </value>
        public int Price
        {
            get
            {
                return _price;
            }

            set
            {
                if (value > 0 && value < 999999999)
                {
                    _price = value;
                }
            }
        }
    }
}
