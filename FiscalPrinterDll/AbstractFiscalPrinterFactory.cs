//-----------------------------------------------------------------------
// <copyright file="AbstractFiscalPrinterFactory.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll
{
    /// <summary>
    /// The abstract class describes the fiscal printer factory
    /// </summary>
    public abstract class AbstractFiscalPrinterFactory
    {
        /// <summary>
        /// Creates Elzab fiscal printer.
        /// </summary>
        /// <returns>Elzab instance</returns>
        public abstract FiscalPrinter CreateElzabFiscalPrinter(Elzab.TypeConnection typeConnection, string ip, int port);

        /// <summary>
        /// Creates Posnet fiscal printer.
        /// </summary>
        /// <returns>Posnet instance</returns>
        public abstract FiscalPrinter CreatePosnetFiscalPrinter();

        /// <summary>
        /// Creates Novitus fiscal printer.
        /// </summary>
        /// <returns>Novitus instance</returns>
        public abstract FiscalPrinter CreateNovitusFiscalPrinter();

        /// <summary>
        /// Creates Emar fiscal printer.
        /// </summary>
        /// <returns>Emar instance</returns>
        public abstract FiscalPrinter CreateEmarFiscalPrinter();
    }
}