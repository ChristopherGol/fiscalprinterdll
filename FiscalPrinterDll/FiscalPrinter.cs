﻿//-----------------------------------------------------------------------
// <copyright file="FiscalPrinter.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll
{
    using System;
    using System.Collections.Generic;
    using Model.Invoice;
    using Model.Product;

    /// <summary>
    /// Abstract class describes fiscal printer instance
    /// </summary>
    public abstract class FiscalPrinter
    {
        /// <summary>
        /// The COM port communication with fiscal printer
        /// </summary>
        private int _port;

        /// <summary>
        /// Gets or sets the COM port communication with fiscal printer(property to port).
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port
        {
            get
            {
                return _port;
            }

            set
            {
                if (value >= 0 && value < 255)
                {
                    _port = value;
                }
            }
        }

        /// <summary>
        /// The IP fiscal printer in the network
        /// </summary>
        private string _ip;

        /// <summary>
        /// Gets or sets the IP fiscal printer in the network (property to ip).
        /// </summary>
        /// <value>
        /// The ip.
        /// </value>
        public string IP { get; set; }
        
        /// <summary>
        /// The TCP port fiscal printer to communication with fiscal printer
        /// </summary>
        private int _tcpPort;

        /// <summary>
        /// Gets or sets the TCP port fiscal printer to communication with fiscal printer (property to tcpPort).
        /// </summary>
        /// <value>
        /// The TCP port.
        /// </value>
        public int TcpPort { get; set; } = 1001;

        /// <summary>
        /// Gets or sets the speed communication with fiscal printer (property to speed).
        /// </summary>
        /// <value>
        /// The speed.
        /// </value>
        public int Speed { get; set; } = 9600;

        /// <summary>
        /// Gets or sets the timeout sending and receiving message from/to fiscal printer (property to timeout)
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public int Timeout { get; set; } = 5;

        /// <summary>
        /// Gets or sets the name of the device.
        /// </summary>
        /// <value>
        /// The name of the device.
        /// </value>
        public string DeviceName { get; set; }

        /// <summary>
        /// Gets or sets the device application version.
        /// </summary>
        /// <value>
        /// The device application version.
        /// </value>
        public string DeviceAppVersion { get; set; }

        /// <summary>
        /// Gets or sets the device unique number.
        /// </summary>
        /// <value>
        /// The device unique number.
        /// </value>
        public string UniqueNumber { get; set; }

        /// <summary>
        /// Searches the open COM port.
        /// </summary>
        public abstract void SearchOpenComPort();

        /// <summary>
        /// Open the connection with the fiscal device over the network.
        /// </summary>
        public abstract void OpenIPConnection();

        /// <summary>
        /// Prints the receipt.
        /// </summary>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnReceipt">The message on receipt.</param>
        /// <param name="percentageDiscount">The percentage discount on the receipt</param>
        public abstract void PrintReceipt(List<Product> listProducts, object messageOnReceipt, int percentageDiscount);

        /// <summary>
        /// Prints the invoice.
        /// </summary>
        /// <param name="numberInvoice">The number invoice.</param>
        /// <param name="company">The company instance.</param>
        /// <param name="listProducts">The list products.</param>
        /// <param name="messageOnInvoice">The message on invoice.</param>
        /// <param name="percentageDiscount">The percentage discount on the receipt</param>
        public abstract void PrintInvoice(string numberInvoice, Company company, List<Product> listProducts, object messageOnInvoice, int percentageDiscount);

        /// <summary>
        /// Print days the report.
        /// </summary>
        /// <param name="ifSale">If sale.</param>
        public abstract void DayReport(object ifSale);

        /// <summary>
        /// Prints the report by periods.
        /// </summary>
        /// <param name="type">The type report(detailed or shortened).</param>
        /// <param name="time1">Since when</param>
        /// <param name="time2">Until when</param>
        public abstract void PeriodReport(object type, DateTime time1, DateTime time2);

        /// <summary>
        /// Prints the report by numbers.
        /// </summary>
        /// <param name="type">The type report(detailed or shortened).</param>
        /// <param name="startNumber">The start number.</param>
        /// <param name="endNumber">The end number.</param>
        public abstract void NumberReport(object type, int startNumber, int endNumber);

        /// <summary>
        /// Prints month the report.
        /// </summary>
        /// <param name="type">The type report(detailed or shortened).</param>
        /// <param name="month">The month and the year.</param>
        public abstract void MonthReport(object type, DateTime month);

        /// <summary>
        /// Gets the fiscal printer time.
        /// </summary>
        /// <returns>The fiscal printer time</returns>
        public abstract DateTime GetDeviceTime();

        /// <summary>
        /// Changes the time in fiscal printer.
        /// </summary>
        /// <param name="clock">The clock.</param>
        public abstract void ChangeTime(DateTime clock);

        /// <summary>
        /// Gets the unique number fiscal printer (read-only)
        /// </summary>
        public abstract void GetUniqueNumberDevice();

        /// <summary>
        /// Opens the drawer.
        /// </summary>
        public abstract void DrawerOpen();
    }
}
