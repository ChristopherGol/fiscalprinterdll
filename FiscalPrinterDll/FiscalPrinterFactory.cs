﻿//-----------------------------------------------------------------------
// <copyright file="FiscalPrinterFactory.cs" company="Krzysztof Goljasz">
//     Copyright (c) Krzysztof Goljasz. All rights reserved.
// </copyright>
// <author>Krzysztof Goljasz</author>
//-----------------------------------------------------------------------

namespace FiscalPrinterDll
{
    using System;
    using Elzab;

    /// <summary>
    /// The fiscal printer abstract factory method
    /// Use abstract factory method pattern
    /// </summary>
    /// <seealso cref="FiscalPrinterDll.AbstractFiscalPrinterFactory" />
    public class FiscalPrinterFactory : AbstractFiscalPrinterFactory
    {
        /// <summary>
        /// Creates Elzab fiscal printer.
        /// </summary>
        /// <returns>
        /// Elzab instance
        /// </returns>
        public override FiscalPrinter CreateElzabFiscalPrinter(Elzab.TypeConnection typeConnection, string ip, int port)
        {
            if (typeConnection == TypeConnection.COM)
            {
                ElzabFiscalPrinter elzabFiscalPrinter = new ElzabFiscalPrinter(port);
                elzabFiscalPrinter.SearchOpenComPort();

                return elzabFiscalPrinter;
            } 
            else if (typeConnection == TypeConnection.Network)
            {
                ElzabFiscalPrinter elzabFiscalPrinter = new ElzabFiscalPrinter(ip);
                elzabFiscalPrinter.OpenIPConnection();

                return elzabFiscalPrinter;
            }
            else
            {
                throw new Exception("Błąd połączenia z urządzeniem! Spróbuj ponownie.");
            }
            
        }

        /// <summary>
        /// Creates Emar fiscal printer (not yet implemented).
        /// </summary>
        /// <returns>
        /// Emar instance
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not yet implemented.</exception>
        public override FiscalPrinter CreateEmarFiscalPrinter()
        {
            throw new NotImplementedException("Not yet implemented Emar fiscal printer methods.");
        }

        /// <summary>
        /// Creates Novitus fiscal printer (not yet implemented).
        /// </summary>
        /// <returns>
        /// Novitus instance
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not yet implemented.</exception>
        public override FiscalPrinter CreateNovitusFiscalPrinter()
        {
            throw new NotImplementedException("Not yet implemented Novitus fiscal printer methods.");
        }

        /// <summary>
        /// Creates Posnet fiscal printer (not yet implemented).
        /// </summary>
        /// <returns>
        /// Posnet instance
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not yet implemented.</exception>
        public override FiscalPrinter CreatePosnetFiscalPrinter()
        {
            throw new NotImplementedException("Not yet implemented Posnet fiscal printer methods.");
        }
    }
}
