# FiscalPrinterDll .NET

FiscalPrinterDll is .NET library that allows you to work with fiscal printer. You can attach library to .NET project and enjoy the simple fiscal printer support in your application.

## Getting Started

FiscalPrinterDll supports fiscal printer:

* Elzab
* Posnet (not yet implemented)
* Novitus (not yet implemented)
* Emar (not yet implementated)

The library allow to:

* print receipts
* print receipts with discount on every position/entire receipt
* print invoices
* print daily report
* print monthly report
* print period report by time/number
* print locked products
* print non-fiscal printouts e.g car wash ticket, confirmation of payment, qr code etc.
* read/write time on device
* read/write tax rates
* print additional lines after receipt logo
* remotely turn off the device
* create own functions to support device

### Prerequisites

* Download Elzab library from [Elzab](ftp://ftp.elzab.com.pl/kody/elzabdr_test.zip), extract files and copy elzabdr.dll to /bin/Debug/ directory.

* If you want to create own functions to support device, you must read [Developer's manual](ftp://ftp.elzab.com.pl/kody/om_iprg.zip). Extract files and open *Protokół komunikacyjny ElzabESC dla drukarek ELZAB Mera, Omega, Mera E, EFV, TE, ZETA i D10.pdf*. 


### Installing

* Clone project to local machine
* Open project in Visual Studio 
* Compile FiscalPrinterDll project to library (.dll) and add library in your project (references)
* Use many methods to work with different fiscal printers (Please read files from Documentation directory)

## Running the tests

You can run unit tests (for that moment only Elzab) that are located in FiscalPrinterDll/Elzab/Tests directory.
The unit test is divided on three parts:

1. Unit tests with connected fiscal printer (fiscal prints for example receipt, daily report, monthly report etc.)
2. Unit tests with disconnected fiscal printer (fiscal prints for example receipt, daily report, monthly report etc.)
3. Unit tests with connected printer (non-fiscal prints for example car wash ticket, phone recharge etc.)

### And coding style tests

The code was tested with use [StyleCop](https://stylecop.codeplex.com/) and [ReSharper](https://www.jetbrains.com/resharper/). 

## Deployment

It can compile project and add reference (.dll) to your project. The library is PCL (portable class library) that you can add reference to:

* ASP .NET Core 1.0
* .NET Framework 4.5
* Windows 8

## Versioning

The current version is 1.0.0. 

## Authors

* **Krzysztof Goljasz** - *Architecture, implementation, tests, documentation project* - [ChristopherGol](https://bitbucket.org/ChristopherGol/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
